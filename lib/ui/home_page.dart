import 'package:flutter/material.dart';
import 'package:sound2melody/controller/note_detector.dart';
import 'package:assets_audio_player/assets_audio_player.dart';


class HomePage extends StatefulWidget {
  HomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  HomePageState createState() => HomePageState();
}

class HomePageState extends State<HomePage>{

  static List<String> fileTests = ['Choose file','test.wav', 'test1M.wav', 'A96.wav', 'A4.wav', '100Hz.wav', '250Hz.wav', 'c1.wav', 'orig4.wav'
                                    , 'piano_A0.wav', 'Db.wav', 'Gb.wav', 'Ab.wav', 'G.wav', 'Audio.wav'];
  String _fileName = fileTests[0];
  // Create dropdown to select file test
  Widget createDropDown() {
    return DropdownButton<String>(
        items: fileTests.map((String val) {
          return new DropdownMenuItem<String>(
              value: val,
              child: Container(
                width: MediaQuery.of(context).size.width * 0.35,
                child: SingleChildScrollView(
                  child: new Text(val),
                ),
              )
          );
        }).toList(),
        value: _fileName,
        hint: Text("Choose file"),
        onChanged: (newVal) {
          _fileName = newVal;
          print('File name is ' + _fileName);
          this.setState(() {});
        });
  }

  Future<void> detectNoteFromAsset() async{
    NoteDetector nodeDetector = new NoteDetector();
    _result = await nodeDetector.detectFromAsset(_fileName);
    this.setState(() {});
  }


  AssetsAudioPlayer assetsAudioPlayer;
  void playAssetFile(){
    if(assetsAudioPlayer != null) {
      if(assetsAudioPlayer.isPlaying.value)
        assetsAudioPlayer.stop();
      assetsAudioPlayer.dispose();
    }
    assetsAudioPlayer = AssetsAudioPlayer();
    assetsAudioPlayer.open(
      Audio("assets/" + _fileName),
    );
//    assetsAudioPlayer.playOrPause();
    assetsAudioPlayer.play();
  }

  // Add buttons to analyse or play the test file
  Widget _analysingSample() => Row(
    mainAxisAlignment: MainAxisAlignment.center,
    children: <Widget>[
      createDropDown(), 
      new RaisedButton(
          onPressed: () {detectNoteFromAsset(); print('detct note donne...');},
          child: new Text("Analyse"),
        ),
      new RaisedButton(
          onPressed: () => playAssetFile(),
          child: new Text('Play'),
      )
    ],
  );

  //create Record button
  String _recordingTitle = "Record";
  Widget _buttonRecording() => Padding(
      padding: const EdgeInsets.only(top: 5.0, bottom: 10.0),
      child:
      new RaisedButton(
        onPressed: () => startRecording(),
        child: new Text(
          _recordingTitle,
          style: TextStyle(color: Color.fromARGB(255, 255, 0, 0)),
        ),
      )
  );

  bool _isRecording = false;
  void startRecording(){
    if(_isRecording){
      _isRecording = false;
      _recordingTitle = "Record";
    } else {
      _isRecording = true;
      _recordingTitle = "Stop";
    }
    this.setState(() {});
  }

  // show the result
  String _result;
  Widget _buildTextResult() => Padding(
      padding: const EdgeInsets.only(left: 5.0, right: 5.0),
      child: Container(
          height: MediaQuery.of(context).size.height * 0.6,
          child:SingleChildScrollView(
            //child: new Padding(
            child: new Text(
                _result ??
                        '''
          Show the result...
          '''
            ),
            //)
          )
      )
  );

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text('Sound2Melogy'),
      ),
      body: new Column(
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          new Column(
              children: <Widget>[
                _analysingSample(),
                _buttonRecording(),
                _buildTextResult()
              ]
          ),
        ],
      ),
      //),
    );
  }
}