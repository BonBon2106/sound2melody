import 'package:sound2melody/model/frequency_to_note.dart';
import 'package:sound2melody/model/note.dart';
import 'package:sound2melody/model/cepstrum_note_detector.dart';
import 'package:sound2melody/model/yin_note_detector.dart';
import 'package:sound2melody/model/mcleod_note_detector.dart';
import 'package:sound2melody/model/wav_io.dart';

class NoteDetector{
  NoteDetector();

  Future<String> detectFromAsset(String fileName) async{
    WaveIO wavIO = new WaveIO();
    var PCMs = await wavIO.getPCMFromAsset('assets/' + fileName);
    var numberOfSamples = 2048;
    var freq2Note = new FrequencyToNote(wavIO.getSampleRate());
    List<double> tmpPCMs = new List(numberOfSamples);
    var freq;
    Note note;
    var result = '';
    var startTime = new DateTime.now();
    print(startTime.toUtc().toString());
    if(true){//cepstrum
      var cspNoteDetector = new CepstrumNoteDetector(wavIO.getSampleRate());
      for(int i = 0; i < PCMs.length - numberOfSamples; ){
        tmpPCMs = PCMs.sublist(i, i+numberOfSamples);
        freq = cspNoteDetector.detectPitch(tmpPCMs, wavIO.getSampleRate());
        note = freq2Note.getNote(freq);
        if(note != null){
          result += note.Name + " : " + note.FundamentalFrequency.toString() + "\r\n";
        }
        i+=numberOfSamples;
      }
    } else if (false) { //YIN method
      var noteDetector = new YINNoteDetector(wavIO.getSampleRate(), numberOfSamples);
//        print('init Note Detector done');
      int countOfSamples = 0;
      int i = 0;
      for (i = 0; i < PCMs.length - numberOfSamples;) {
        tmpPCMs = PCMs.sublist(i, i + numberOfSamples);
        //print(tmpPCMs[0].toString());
        freq = noteDetector.detect(tmpPCMs);
        note = freq2Note.getNote(freq);
//          print('Note is ' + note.Name);
        if(note != null)
          result += note.Name + " : " + note.FundamentalFrequency.toString() + "\r\n";
        countOfSamples = i + numberOfSamples;
        i += numberOfSamples;
      }
    } else {//MPM method
      var mpmMethod = new McleodNoteDetector(wavIO.getSampleRate(), numberOfSamples);
      var countNote = 0;
      for (int i = 0; i < PCMs.length - numberOfSamples;) {
        tmpPCMs = PCMs.sublist(i, i + numberOfSamples);
        freq = mpmMethod.getPitch(tmpPCMs);
//          print('freq is ' + freq.toString() + " with i=" + i.toString());
        note = freq2Note.getNote(freq);
        if(note != null) {
//            print('Note is ' + note.Name);
          result += note.Name + " : " + note.FundamentalFrequency.toString() + "\r\n";
          countNote++;
        }
        i += numberOfSamples;
      }
    }
    var endTime = new DateTime.now();
    var diffTime = endTime.difference(startTime);
    print('done: ' + endTime.toUtc().toString() + " => " + diffTime.inSeconds.toString() + " seconds");
    return wavIO.getFileInfo() + "\r\n" + result;
  }
}