
import 'dart:math';

class Note {
  String _mName;
  double _mFrequency;

  double _mLowBorder;
  double _mUpBorder;

  double _mDiffToNext;
  double _mDiffToPrev;

  Note(double frequency, String name)
  {
    _mFrequency = frequency;
    _mName = name;
    double factor = pow(2, (1 / 12));

    var prevNote = frequency / factor;
    _mDiffToPrev = frequency - prevNote;
    _mLowBorder = frequency - _mDiffToPrev / 2;

    var nextNote = frequency * factor;
    _mDiffToNext = nextNote - frequency;
    _mUpBorder = frequency + _mDiffToNext / 2;
  }

  /// <summary>
  /// Name of Note: C, C#, D, D#, E, F, F#, G, G#, A, A#, B
  /// </summary>
  String get Name => _mName;
  void set Name(String name) {
    _mName = name;
  }

  /// get, set the fundamental frequency of note
  double get FundamentalFrequency => _mFrequency;
  void set FundamentalFrequency (double frequency)
  {
    _mFrequency = frequency;
  }

  /// low border of fundamental frequency
  double get LowBorder => _mLowBorder;
  void set LowBorder (double frequency)
  {
    _mLowBorder = frequency;
  }

  /// up border of fundamental frequency
  double get UpBorder => _mUpBorder;
  void set UpBorder (double frequency)
  {
    _mUpBorder = frequency;
  }

  double get DiffToNext => _mDiffToNext;
  void set DiffToNext (double value)
  {
    _mDiffToNext = value;
  }

  double get DiffToPrev => _mDiffToPrev;
  void set DiffToPrev (double value)
  {
    _mDiffToNext = value;
  }
}