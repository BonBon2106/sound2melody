import 'dart:math';

import 'package:fft/fft.dart';
import 'package:my_complex/my_complex.dart';

/// Detect Pitch in frequency domain
/// https://ccrma.stanford.edu/~jos/sasp/Quadratic_Interpolation_Spectral_Peaks.html
class CepstrumNoteDetector {
  final String tag = "CepstrumNoteDetector";

  List<double> a, b, mem1, mem2;

  CepstrumNoteDetector(int sampleRate){
    a = new List(2);
    b = new List(3);
    mem1 = new List(4);
    mem2 = new List(4);
    computeSecondOrderLowPassParameters(sampleRate, 440, a, b);
    for (int i = 0; i < 4; i++){
      mem1[i] = 0;
      mem2[i] = 0;
    }
  }

  List<double> hammingWindow (List<double> data, int windowSize){
    List<double> result = List(windowSize);
    List<double> window = List(windowSize);
    for(int i = 0; i < windowSize; i++){
      window[i] = 0.54 - 0.46 * cos(2 * pi * i / windowSize);
    }
    for(int j = 0; j < windowSize; j++) {
      result[j] = data[j] * window[j];
    }
    return result;
  }

  List<double> buildHanWindow( List<double> data, int windowSize )
  {
    List<double> result = new List(windowSize);
    List<double> window = List(windowSize);
    for( int i=0; i<windowSize; ++i )
      window[i] = .5 * ( 1 - cos( 2 * pi * i / (windowSize-1.0) ) );
    for(int j = 0; j < windowSize; j++) {
      result[j] = data[j] * window[j];
    }
    return result;
  }

  void computeSecondOrderLowPassParameters( int srate, double f, List<double> a, List<double> b )
  {
    double a0;
    double w0 = 2 * pi * f/srate;
    double cosw0 = cos(w0);
    double sinw0 = sin(w0);
    double alpha = sinw0/2 * sqrt(2);

    a0   = 1 + alpha;
    a[0] = (-2*cosw0) / a0;
    a[1] = (1 - alpha) / a0;
    b[0] = ((1-cosw0)/2) / a0;
    b[1] = ( 1-cosw0) / a0;
    b[2] = b[0];
  }

  double processSecondOrderFilter(double x, List<double> mem, List<double> a, List<double> b){
    double ret = b[0] * x + b[1] * mem[0] + b[2] * mem [1] - a[0] * mem[2] - a[1] * mem[3];
    mem[1] = mem[0];
    mem[0] = x;
    mem[3] = mem[2];
    mem[2] = ret;
    return ret;
  }

  /// find peak base on magnitude os spectrum
  double detectPitch(List<double> data, int sampleRate){
    double freq = -1;
    List<double> dataFilter = new List(data.length);
    try {
      for(int i = 0; i < data.length; i++){
        dataFilter[i] = processSecondOrderFilter(data[i], mem1, a, b);
        dataFilter[i] = processSecondOrderFilter(dataFilter[i], mem2, a, b);
      }
      var windowData = hammingWindow(data, data.length);
      //fft
      List<Complex> fft = new FFT().Transform(windowData);

      // calculate all magnitudes
      List<double> magnitude = new List(fft.length);
      for (int i = 0; i < fft.length; i ++) {
        magnitude[i] = (pow(fft[i].real, 2) + pow(fft[i].imaginary, 2));
      }

      //find the peak
      double peak = magnitude[0];
      int peakIndex = 0;
      for (int i = 1; i < magnitude.length; i++) {
        if (magnitude[i] > peak) {
          peak = magnitude[i];
          peakIndex = i;
        }
      }
      // Parabolic approximation is necessary to get the exactly frequency of a discrete signal
      // since the frequency can be in some point between the samples.
      var peakIndexDb = parabolic(arrayLog(magnitude), peakIndex);

      //calculate the freq
      freq = peakIndexDb * sampleRate / data.length;
      //print('freq is ' + freq.toString());
    } catch (e) {
      print(tag + ": Can not find pitch " + e.toString());
      freq = -1;
    }
    return freq;
  }
  // Compute the natural logarithm for each element of the array
  List<double> arrayLog(List<double> a) {
    List<double> c = new List(a.length);
    for (int i = 0; i < a.length; i++) {
      c[i] = log(a[i]); //fixme change to fast log in the future
    }
    return c;
  }

  // Quadratic interpolation for estimating the true position of an
  // inter-sample maximum when nearby samples are known.
  //  [y] : is a vector
  //  [x] : is an index point for that vector
  double parabolic(List<double> y, int x) {
    var xv;
    var yv;
    if(x == 0 || x == y.length - 1) {
      return 0;
    } else {
      xv = 1 / 2.0 * (y[x - 1] - y[x + 1]) / (y[x - 1] - 2 * y[x] + y[x + 1]) +
              x;
      yv = y[x] - 1 / 4.0 * (y[x - 1] - y[x + 1]) * (xv - x);
    }
    return xv;
  }
}