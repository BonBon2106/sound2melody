import 'dart:math';

/// Using YIN algorithm to determine the frequency of the provided waveform data.
/// in time domain
/// "http://recherche.ircam.fr/equipes/pcm/cheveign/ps/2002_JASA_YIN_proof.pdf"
class YINNoteDetector {
  /// The default YIN threshold value. Should be around 0.10~0.15. See YIN
  /// paper for more information.
  final ABSOLUTE_THRESHOLD = 0.125;
  int _sampleRate;
  List<double> _resultBuffer;

  YINNoteDetector (int sampleRate, int bufferSize){
    _sampleRate = sampleRate;
    _resultBuffer = new List<double>.filled(bufferSize ~/ 2, 0, growable: true);
    print('init NodeDetector object with sampleRate is ' + sampleRate.toString()
        + " and bufferSize=" + _resultBuffer.length.toString());
  }
  /// Implements the difference function as described in step 2 of the YIN
  /// paper (time domain)
  void autoCorrelationDifference (List<double> pcm)
  {
    int length = _resultBuffer.length;
//    print('autoCorrelationDifference: buffer length is ' + length.toString());
    int i, j;
    for(j = 1; j < length; j++){
      for (i = 0; i < length; i++){
//        print(i.toString());
        try {
          _resultBuffer[j] += (pcm[i] - pcm[i+j]) * (pcm[i] - pcm[i+j]) ;//pow((pcm[i] - pcm[i + j]), 2);
        } catch (e) {
          print(i.toString() + " + " + j.toString());
          print(pcm[i].toString() + "-" + pcm[i+j].toString());
          print(e.toString() + ": " + i.toString() + "; " + pcm[i].toString() + "; " + _resultBuffer[j].toString());
          return;
        }
      }
//      print('autoCorrelationDifference: _resultBuffer[' + j.toString() + "]=" + _resultBuffer[j].toString());
    }
//    print('autoCorrelationDifference: end');
  }

  /// The cumulative mean normalized difference function as described in step 3
  /// of the YIN paper. <br>
  /// <code>
  /// _resultBuffer[0] == _resultBuffer[1] = 1
  /// </code>
  void cumulativeMeanNormalizedDifference (){
    int i;
    int length = _resultBuffer.length;
    double runningSum = 0;
    // Set the first value in the result buffer to 1
    _resultBuffer[0] = 1;

    for (i = 1; i < length; i++){
      // The sum of this value plus all the previous values in the buffer array
      runningSum += _resultBuffer[i];
      // update the current value
      _resultBuffer[i] *= i / runningSum;
//      print('cumulativeMeanNormalizedDifference: _resultBuffer[' + i.toString() + "]=" + _resultBuffer[i].toString());
    }
  }
  /// Implements step 4 of the AUBIO_YIN paper
  int absoluteThreshold (){
    int tau;
    int length = _resultBuffer.length;

    // The first 2 values in the buffer should be 1.
    for (tau = 2; tau < length; tau++){
//      print('buffer[' + tau.toString() + "]=" + _resultBuffer[tau].toString());
      if(_resultBuffer[tau] < ABSOLUTE_THRESHOLD){
//        print('finding the lowest value');
        while(tau + 1 < length && _resultBuffer[tau + 1] < _resultBuffer[tau]){
          tau++;
        }
        break;
      }
    }
//    print('absoluteThreshold: tau is ' + tau.toString());
    tau = tau >= length ? length - 1 : tau;
    return tau;
  }

  /// Implements step 5 of the AUBIO_YIN paper. It refines the estimated tau
  /// value using parabolic interpolation. This is needed to detect higher
  /// frequencies more precisely. See http://fizyka.umk.pl/nrbook/c10-2.pdf and
  /// for more background
  /// http://fedc.wiwi.hu-berlin.de/xplore/tutorials/xegbohtmlnode62.html
  ///
  /// @param tauEstimate
  ///            The estimated tau value.
  /// @return A better, more precise tau value.
  double parabolicInterpolcation(final int currentTau) {
    // Finding the points to fit the parabola between
    int x0 = currentTau < 1 ? currentTau : currentTau - 1;
    int x2 = currentTau + 1 < _resultBuffer.length ? currentTau + 1 : currentTau;

    // Finding the better tau estimate
    double betterTau;
    if(x0 == currentTau) {
      if(_resultBuffer[currentTau] <= _resultBuffer[x2])
        betterTau = currentTau.toDouble();
      else
        betterTau = x2.toDouble();
    } else if (x2 == currentTau) {
      if(_resultBuffer[currentTau] <= _resultBuffer[x0])
        betterTau = currentTau.toDouble();
      else
        betterTau = x0.toDouble();
    } else {
      var s0 = _resultBuffer[x0];
      var s1 = _resultBuffer[currentTau];
      var s2 = _resultBuffer[x2];
      betterTau = currentTau + (s2 - s0) / (2 * (2 * s1 - s2 - s0));
    }
    return betterTau;
  }

  double detect (List<double> pcm){
    int tau;
//    print('detect with size of buffer is ' + pcm.length.toString());
    autoCorrelationDifference(pcm);
//    print('audoCorrelationDiff done');
    cumulativeMeanNormalizedDifference();
//    print('cumulative...');
    tau = absoluteThreshold();
//    print('tau is ' + tau.toString());
    var betterTau = parabolicInterpolcation(tau);
//    print('bettertau is ' + betterTau.toString());
    // The fundamental frequency is the sampling rate divided by the tau. The period is the duration of one cycle.
    // Frequency = SampleRate / Period
    return _sampleRate/betterTau;
  }
}