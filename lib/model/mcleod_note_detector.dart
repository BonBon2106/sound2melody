import 'dart:math';

/// Mcleod algorithm
/// in time domain
/// http://miracle.otago.ac.nz/tartini/papers/A_Smarter_Way_to_Find_Pitch.pdf
class McleodNoteDetector {
  int _bufferSize;
  int _sampleRate;
  List<double> _resultBuffer;
  /// For performance reasons, peaks below this cutoff are not even considered
  final double SMALL_CUTOFF = 0.5;
  // Defines the relative size the chosen peak (pitch) has. 0.93 means: choose
  // the first peak that is higher than 93% of the highest peak detected. 93%
  // is the default value used in the Tartini user interface.
  final double DEFAULT_CUTOFF = 0.97;
  // Pitch annotations below this threshold are considered invalid, they are
  // ignored
  final double LOWER_PITCH_CUTOFF = 16.0;// Hz
  double _cutoff;
  List<double> nsdf;
  double turningPointX, turningPointY;
  List<int> maxPositions;
  List<double> periodEstimates;
  List<double> ampEstimates;

  McleodNoteDetector(int sampleRate, int bufferSize){
    _sampleRate = sampleRate;
    _resultBuffer = new List<double>.filled(bufferSize ~/ 2, 0, growable: true);
    _cutoff = DEFAULT_CUTOFF;
    maxPositions = new List();
    periodEstimates = new List();
    ampEstimates = new List();
    nsdf = new List(bufferSize);
  }

  // Implements the normalized square difference function. See section 4 (and
  // the explanation before) in the MPM article. This calculation can be
  // optimized by using an FFT. The results should remain the same.
  void normalizedSquareDifference(List<double> audioBuffer){
    for (int tau = 0; tau < audioBuffer.length; tau++) {
      double acf = 0;
      double divisorM = 0;
      for (int i = 0; i < audioBuffer.length - tau; i++) {
        acf += audioBuffer[i] * audioBuffer[i + tau];
        divisorM += pow(audioBuffer[i], 2) + pow(audioBuffer[i + tau], 2);
      }
      nsdf[tau] = 2 * acf / divisorM;
    }
  }

  // Finds the x value corresponding with the peak of a parabola
  void prabolicInterpolation(int tau) {
//    print('prabolicInterpolation....');
    double nsdfa = nsdf[tau - 1];
    double nsdfb = nsdf[tau];
    double nsdfc = nsdf[tau + 1];
    double bValue = tau.toDouble();
    double bottom = nsdfc + nsdfa - 2 * nsdfb;
    if (bottom == 0.0) {
      turningPointX = bValue;
      turningPointY = nsdfb;
    } else {
      final double delta = nsdfa - nsdfc;
      turningPointX = bValue + delta / (2 * bottom);
      turningPointY = nsdfb - delta * delta / (8 * bottom);
    }
//    print('prabolicInterpolation...done');
  }

  // Finds the highest value between each pair of positive zero crossings.
  // Including the highest value between the last positive zero crossing and
  // the end (if any). Ignoring the first maximum (which is at zero)
  void peakPicking() {

    int pos = 0;
    int curMaxPos = 0;
//    print ("peakPicking...");
    // find the first negative zero crossing
    while (pos < (nsdf.length - 1) / 3 && nsdf[pos] > 0) {
      pos++;
    }

//    print ("peakPicking...1");

    // loop over all the values below zero
    while (pos < nsdf.length - 1 && nsdf[pos] <= 0.0) {
      pos++;
    }

//    print ("peakPicking...2");
    // can happen if output[0] is NAN
    if (pos == 0) {
      pos = 1;
    }

    while (pos < nsdf.length - 1) {
      //assert nsdf[pos] >= 0;
      if(nsdf[pos] < 0) {
        print('peakPicking: invalid data nsdf' + nsdf[pos].toString());
        break;
      }
      if (nsdf[pos] > nsdf[pos - 1] && nsdf[pos] >= nsdf[pos + 1]) {
        if (curMaxPos == 0) {
          // the first max (between zero crossings)
          curMaxPos = pos;
        } else if (nsdf[pos] > nsdf[curMaxPos]) {
          // a higher max (between the zero crossings)
          curMaxPos = pos;
        }
      }
      pos++;
      // a negative zero crossing
      if (pos < nsdf.length - 1 && nsdf[pos] <= 0) {
        // if there was a maximum add it to the list of maxima
        if (curMaxPos > 0) {
          maxPositions.add(curMaxPos);
          curMaxPos = 0; // clear the maximum position, so we start
          // looking for a new ones
        }
        while (pos < nsdf.length - 1 && nsdf[pos] <= 0) {
          pos++; // loop over all the values below zero
        }
      }
    }
    if (curMaxPos > 0) { // if there was a maximum in the last part
      maxPositions.add(curMaxPos); // add it to the vector of maxima
    }
//    print ("peakPicking...done");
  }

  double getPitch (List<double> audioBuffer) {
    double pitch;

    // 0. Clear previous results (Is this faster than initializing a list
    // again and again?)
    maxPositions.clear();
    periodEstimates.clear();
    ampEstimates.clear();

    // 1. Calculate the normalized square difference for each Tau value.
    normalizedSquareDifference(audioBuffer);
    // 2. Peak picking time: time to pick some peaks.
    peakPicking();

    double highestAmplitude = nsdf[maxPositions[0]];
//    print('getPitch...0');
    maxPositions.forEach((tau) {
      // make sure every annotation has a probability attached
      highestAmplitude = max(highestAmplitude, nsdf[tau]);

      if (nsdf[tau] > SMALL_CUTOFF) {
        // calculates turningPointX and Y
        prabolicInterpolation(tau);
        // store the turning points
        ampEstimates.add(turningPointY);
        periodEstimates.add(turningPointX);
        // remember the highest amplitude
        highestAmplitude = max(highestAmplitude, turningPointY);
      }
    });

//    print('getPitch...');

    if (periodEstimates.isEmpty) {
      pitch = -1;
    } else {
      // use the overall maximum to calculate a cutoff.
      // The cutoff value is based on the highest value and a relative
      // threshold.
      final double actualCutoff = _cutoff * highestAmplitude;

      // find first period above or equal to cutoff
      int periodIndex = 0;
      for (int i = 0; i < ampEstimates.length; i++) {
        if (ampEstimates[i] >= actualCutoff) {
          periodIndex = i;
          break;
        }
      }
//      print('getPitch...2');
      final double period = periodEstimates[periodIndex];
      final double pitchEstimate = (_sampleRate / period);
      if (pitchEstimate > LOWER_PITCH_CUTOFF) {
        pitch = pitchEstimate;
      } else {
        pitch = -1;
      }
//      print('getPitch...done');
    }
    return pitch;
  }

  double getPitchFromShort (List<int> data) {
    List<double> doubleData = new List(data.length);
    int maxshort = 0;
    double freq = 0;
    for (int i = 0; i < data.length; i++) {
      if(data[i] > maxshort) {
        maxshort = data[i];
      }
    }
    for (int i = 0; i < data.length; i++) {
      doubleData[i] = data[i] * 32768 / maxshort;
    }
    freq = getPitch(doubleData);
//    print('getPitchFromShort: freq is ' + freq.toString());
    return freq;
  }
}