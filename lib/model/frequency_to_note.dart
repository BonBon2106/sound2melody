import 'dart:math';
import 'note.dart';

/// Convert fundamental Frequency To Note
class FrequencyToNote {
  List<String> keyLetters = ["A", "A#", "B", "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#"];
  int startCode = 12; ///Code of C0
  int endCode = 155; /// Code of B12
  List<Note> mNotes;
  int _sampleRate;

  //initialize
  FrequencyToNote(int sampleRate)
  {
    int capacity = endCode - startCode + 1;
    mNotes = new List(capacity);
    for (int index = startCode; index <= endCode; index++)
    {
      var freq = code2Frequency(index);
      String name = code2Name(index);
      mNotes[index - 12] = new Note(freq, name);
      //print(name + " - " + freq.toString());
    }
    _sampleRate = sampleRate;
  }
  /// Convert code to frequency
  double code2Frequency(int code)
  {
    return 27.5 * pow(2, (code - 21) / 12);
  }
  /// Convert code to name
  String code2Name(int code)
  {
    String name = "";
    // Note
    switch (code % 12)
        {
      case 0:
        name = "C";
        break;
      case 1:
        name = "C#";
        break;
      case 2:
        name = "D";
        break;
      case 3:
        name = "D#";
        break;
      case 4:
        name = "E";
        break;
      case 5:
        name = "F";
        break;
      case 6:
        name = "F#";
        break;
      case 7:
        name = "G";
        break;
      case 8:
        name = "G#";
        break;
      case 9:
        name = "A";
        break;
      case 10:
        name = "A#";
        break;
      case 11:
        name = "B";
        break;

    }
    name += (code ~/ 12 - 1).toString(); //Note number (0,1,2....10, 11)
    return name;
  }

  /// <summary>
  /// Find Note with frequency and range of capacity
  /// </summary>
  /// <param name="frequency"></param>
  /// <param name="startIndex"></param>
  /// <param name="endIndex"></param>
  /// <returns> Note</returns>
  Note getNoteFromRange(double frequency, int startIndex, int endIndex)
  {
    int index = (startIndex + endIndex) ~/ 2;
//      print('GetNoteFromRange: ' + startIndex.toString() + ";" + endIndex.toString());
    if(frequency < mNotes[0].LowBorder || frequency > mNotes[mNotes.length - 1].UpBorder)
      return null;
    if (mNotes[index].LowBorder < frequency && frequency < mNotes[index].UpBorder)
    {
      return mNotes[index];
    }

    if (frequency < mNotes[index].LowBorder)
      return getNoteFromRange(frequency, startIndex, index);
    if (mNotes[index].UpBorder < frequency)
      return getNoteFromRange(frequency, index, endIndex);
    print('GetNoteFromRange: Can not get note: ' + frequency.toString() + ", " + index.toString() + ", " + endIndex.toString());
    return null;

  }
  /// <summary>
  /// Find note with frequency
  /// </summary>
  /// <param name="frequency"></param>
  /// <returns>Note</returns>
  Note getNote(double frequency)
  {
    if(frequency <= 0)
      return null;
    // The sampling rate must be twice the highest frequency of sound
    if(frequency > _sampleRate / 2)
      return null;
    int capacity = endCode - startCode + 1;
//      print('GetNote: freq=' + frequency.toString());
    int index = capacity ~/ 2;
    if (mNotes[index].LowBorder < frequency && frequency < mNotes[index].UpBorder)
    {
      return mNotes[index];
    }
//      print('GetNote: index=' + index.toString() +"; " + mNotes[index].LowBorder.toString() + "; "
//                              + mNotes[index].UpBorder.toString());
    if (frequency < mNotes[index].LowBorder)
      return getNoteFromRange(frequency, 0, index);
    if (mNotes[index].UpBorder < frequency)
      return getNoteFromRange(frequency, index, capacity);
    print('GetNote: Can note get note with frequency is ' + frequency.toString());
    return null;
  }
}