
import 'dart:convert';
import 'dart:async';
import 'dart:typed_data';
import 'package:flutter/services.dart';

class WaveIO {

  String _riffID;           //RIFF: ChunkID 4 bytes
  var _riffSize;		    //In bytes, measured from offset 8, ChunkSize 4 bytes
  String _riffFormat;       //WAVE, usually: Format, 4 bytes

  String _fmtID;    	    //Four bytes: "fmt "
  var _fmtSize;            //Length of header
  var _fmtAudioFormat;  	//1 if uncompressed -> PCM
  var _fmtChannels;      //Number of channels: 1-5
  var _fmtSampleRate;      //In Hz samples per sec
  var _fmtByteRate;        //For estimating RAM allocation dwAvgBytesPerSec
  var _fmtBlockAlign;    //Sample frame size in bytes
  var _fmtBitsPerSample;   //Bits per sample


  var _dataID;    		//Four bytes: "data"
  var _dataSize;    	    //Length of header

  String getStringFromBytes(var bytes, int offset, int length) {
    if(length <= 0 || length >= bytes.length) {
      print('length is invalid, ' + length.toString());
      return '';
    }
    if(offset >= bytes.length || offset < 0) {
      print('offset is invalid, ' + offset.toString());
      return '';
    }

    if(offset + length >= bytes.length) {
      print('out of length with offset=' + offset.toString() + ', len=' + length.toString() + ', bytes.length=' + bytes.length.toString());
      return '';
    }
    List<int> tmpBytes;
    for(int i = 0; i < length; i++){
      if(tmpBytes == null)
        tmpBytes = [bytes.getInt8(i + offset)];
      else
        tmpBytes.add(bytes.getInt8(i + offset));
    }
    return utf8.decode(tmpBytes);
  }

  Future<List<double>> getPCMFromAsset (String filePath) async
  {
    print('get PCM');
    var bytes = await rootBundle.load(filePath);
    var offset = 0;
    //Read header
    //RIFF header 12 bytes
    _riffID = getStringFromBytes(bytes, 0, 4); // 4 bytes
    _riffSize = bytes.getUint32(4, Endian.little); // 4 bytes
    _riffFormat = getStringFromBytes(bytes, 8, 4); // 4 bytes
    //Format header
    _fmtID = getStringFromBytes(bytes, 12, 4); // 4 bytes
    _fmtSize = bytes.getUint32(16, Endian.little); // 4 bytes
    _fmtAudioFormat = bytes.getUint16(20, Endian.little);//2 bytes
    _fmtChannels = bytes.getUint16(22, Endian.little); // 2 bytes
    _fmtSampleRate = bytes.getUint32(24, Endian.little); //4 bytes
    _fmtByteRate = bytes.getUint32(28, Endian.little); // 4 bytes
    _fmtBlockAlign = bytes.getUint16(32, Endian.little); // 2 bytes
    _fmtBitsPerSample = bytes.getUint16(34, Endian.little); // 2 bytes
    offset = _fmtSize + 20; // fmt size (4 bytes) + fmt header (4 bytes) + riff header (12 bytes)
    print('offset is ' + offset.toString() + " sand fmtSize=" + _fmtSize.toString());
    //data header
    while (_dataID != "data") {
      _dataID = getStringFromBytes(bytes, offset, 4);
      _dataSize = bytes.getUint32(offset + 4, Endian.little);
      if(_dataID != "data")
        offset += 8 + _dataSize;
    }

    print('get PCM 1 offset=' + offset.toString());
    var numSamplesPerChannel = (8*_dataSize) ~/ (_fmtChannels * _fmtBitsPerSample);
    print('Number of Samples for 1 channel: ' + numSamplesPerChannel.toString());
    var size_of_each_sample = (_fmtChannels * _fmtBitsPerSample) / 8;
    print('Size of each sample: ' + size_of_each_sample.toString());

    List<double> _leftChannel = new List(numSamplesPerChannel);
    List<double> _rightChannel = new List(numSamplesPerChannel);
    var step = 1; // 1 byte, bitpersample is 8
    if(_fmtBitsPerSample == 8) {
      step = 1;
    } else if (_fmtBitsPerSample == 16) {
      step = 2;
    }
    print('getPCM 2');
    int index = 0;
    print('BitsPerSample=' + _fmtBitsPerSample.toString());
    print('channels=' + _fmtChannels.toString());
//    print(getFileInfo());
    for (; index < numSamplesPerChannel; index++){
//      print('offset=' + offset.toString());
      if(_fmtBitsPerSample == 8) {
        _leftChannel[index] = bytes.getUint8(offset)/256;
      } else {
        _leftChannel[index] = bytes.getInt16(offset, Endian.little)/32768;
      }
      if(_fmtChannels == 2) { //stereo
        offset += step;
      }
      if(_fmtBitsPerSample == 8) {
        _rightChannel[index] = bytes.getUint8(offset)/256;
      } else {
        _rightChannel[index] = bytes.getInt16(offset, Endian.little)/32768;
      }
//     print("_leftChannel[" + index.toString() + "]=" + _leftChannel[index].toString());
      offset += step;
    }
    print('Numget of samples is ' + _leftChannel.length.toString());
    return _leftChannel;
//    return _rightChannel;
  }
  int getSampleRate(){
    return _fmtSampleRate;
  }

  String getFileInfo(){
    String strFileInfo = "__________________________________________\r\n";
    strFileInfo += ("Riff ID: " + _riffID + "\r\n");
    strFileInfo += ("Riff size: " + _riffSize.toString() + "\r\n");
    strFileInfo += ("Riff format: " + _riffFormat + "\r\n");
    strFileInfo += ("Fmt ID: " + _fmtID + "\r\n");
    strFileInfo += ("Fmt size: " + _fmtSize.toString() + "\r\n");
    strFileInfo += ("Fmt audio format: " + _fmtAudioFormat.toString() + "\r\n");
    strFileInfo += ("Fmt channels: " + _fmtChannels.toString() + "\r\n");
    strFileInfo += ("Fmt sample rate: " + _fmtSampleRate.toString() + "\r\n");
    strFileInfo += ("Fmt byte rate: " + _fmtByteRate.toString() + "\r\n");
    strFileInfo += ("Fmt block align: " + _fmtBlockAlign.toString() + "\r\n");
    strFileInfo += ("Fmt bits per sample: " + _fmtBitsPerSample.toString() + "\r\n");
    strFileInfo += ("Data ID: " + _dataID + "\r\n");
    strFileInfo += ("Data size: " + _dataSize.toString() + "\r\n");
    strFileInfo += ("__________________________________________\r\n");
    print(strFileInfo);
    return strFileInfo;
  }
}

